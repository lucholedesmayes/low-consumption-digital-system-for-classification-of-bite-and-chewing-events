//// Q(26,24) Butter 6.5 6to orden
//assign coeffSec2B = {26'h00006BC, 26'h0000D78, 26'h00006BC};
//assign coeffSec2A = {26'h0F8B678, 26'h2076477};

//// Q(26,24) Ellip 5.5 6to orden
//assign coeffSec2B = {26'h009B1A1, 26'h3ECA331, 26'h009B1A1};
//assign coeffSec2A = {26'h0FEF34D, 26'h20119CE};

//// Q(26,24) Butter 5.5 6to orden
//assign coeffSec2B = {26'h00004D5, 26'h00009AA, 26'h00004D5}; //b0, b1, b2
//assign coeffSec2A = {26'h0F9D1FA, 26'h2064159}; //a2, a1

//// Q(26,24) Butter 5 6to orden
//assign coeffSec2B = {26'h00003FF, 26'h00007FF, 26'h00003FF};
//assign coeffSec2A = {26'h0FA5E35, 26'h205AEE6};

// Q(26,24) Butter 4.5 6to orden
assign coeffSec2B = {26'h000033D, 26'h000067B, 26'h000033D};
assign coeffSec2A = {26'h0FAEE63, 26'h2051EB8};


//// Q(26,24) Butter 3 6to orden
//assign coeffSec2B = {26'h0000172, 26'h00002E4, 26'h0000172};
//assign coeffSec2A = {26'h0FC9C4A, 26'h203697E};

