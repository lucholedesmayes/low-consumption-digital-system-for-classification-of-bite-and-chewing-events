//// Q(26,24) Butter 6.5 6to orden
//assign coeffSec3B = {26'h00006B3, 26'h0000D66, 26'h00006B3};
//assign coeffSec3A = {26'h0F618D8, 26'h20A01F3};

//// Q(26,24) Ellip 5.5 6to orden
//assign coeffSec3B = {26'h007CED9, 26'h3F0631A, 26'h007CED9};
//assign coeffSec3A = {26'h0FFCB92, 26'h2004816};

//// Q(26,24) Butter 5.5 6to orden
//assign coeffSec3B = {26'h00004CF, 26'h000099F, 26'h00004CF}; //b0, b1, b2
//assign coeffSec3A = {26'h0F7986C, 26'h2087AD1}; //a2, a1

//// Q(26,24) Butter 5 6to orden
//assign coeffSec3B = {26'h00003FB, 26'h00007F6, 26'h00003FB};
//assign coeffSec3A = {26'h0F85879, 26'h207B4A2};

// Q(26,24) Butter 4.5 6to orden
assign coeffSec3B = {26'h000033A, 26'h0000675, 26'h000033A};
assign coeffSec3A = {26'h0F91D14, 26'h206F006};


//// Q(26,24) Butter 3 6to orden
//assign coeffSec3B = {26'h0000171, 26'h00002E2, 26'h0000171};
//assign coeffSec3A = {26'h0FB618C, 26'h204A437};

