//// Q(26,24) Butter 6.5 6to orden
//assign coeffSec1B = {26'h00006CB, 26'h0000D97, 26'h00006CB};
//assign coeffSec1A = {26'h0FD4EF3, 26'h202CC3B};

//// Q(20,18) Butter 5.5 2do orden
//assign coeffSec1B = {20'h00013, 20'h00026, 20'h00013}; //b0, b1, b2
//assign coeffSec1A = {20'h3E747, 20'h81905}; //a2, a1

//// Q(26,24) Ellip 5.5 6to orden
//assign coeffSec1B = {26'h036353F, 26'h3939B0E, 26'h036353F};
//assign coeffSec1A = {26'h0FF559B, 26'h200B780};

//// Q(26,24) Butter 5.5 6to orden
//assign coeffSec1B = {26'h00004DE, 26'h00009BD, 26'h00004DE}; //b0, b1, b2
//assign coeffSec1A = {26'h0FDB879, 26'h2025B00}; //a2, a1

//// Q(26,24) Butter 5 6to orden
//assign coeffSec1B = {26'h0000406, 26'h000080D, 26'h0000406};
//assign coeffSec1A = {26'h0FDED28, 26'h2021FF2};

// Q(26,24) Butter 4.5 6to orden
assign coeffSec1B = {26'h0000343, 26'h0000686, 26'h0000343};
assign coeffSec1A = {26'h0FE2196, 26'h201EB85};

//// Q(26,24) Butter 3 6to orden
//assign coeffSec1B = {26'h0000173, 26'h00002E7, 26'h0000173};
//assign coeffSec1A = {26'h0FEC10E, 26'h20144BF};