`timescale 100us / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: Sinc(i) - CONICET Sta. Fe
// Engineer: Ledesma Luciano
// 
// Create Date: 14.11.2022 12:24:18
// Design Name: 
// Module Name: Top_system
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module top_system(
    input  [15:0] Signal_in,
    output  [15:0] Signal_out,
    input clk,
    input sync_reset,
    output flag,
    output [15:0] Tk,
    output [15:0] AX2,
    output [15:0] BX2,
    output [15:0] abs_tb,
    output [7:0] b,
    output [7:0] c,
    output [7:0] cb,
    output [7:0] count,
    output [15:0] iir_tb,
    output [15:0] sm_tb,
    output [15:0] fifo_atb,
    output [15:0] fifo_btb,
    output [15:0] fifo_ctb
    );
    
   wire [15:0] abs_to_iir;
   wire [15:0] iir_to_sub; 
   wire [15:0] sub_to_fifo;
   wire [15:0] fifo_to_peak_a;
   wire [15:0] fifo_to_peak_b;
   wire [15:0] fifo_to_peak_c;
   wire [15:0] flag_connect;
   wire [15:0] peak_to_class;
   
   
   wire clk100;
   
   assign abs_tb = abs_to_iir;
   assign iir_tb = iir_to_sub;
   assign sm_tb = sub_to_fifo;
   assign fifo_atb = fifo_to_peak_a;
   assign fifo_btb = fifo_to_peak_b;
   assign fifo_ctb = fifo_to_peak_c;
    
Abs u_abs (
    
    .abs_in (Signal_in),
    .abs_out (abs_to_iir),
    .clk (clk),
    .sync_reset (sync_reset)
    
);
    
iir_top u_iir (

    .i_sample (abs_to_iir), .o_sample (iir_to_sub), .clock (clk),
    .i_reset (sync_reset)

);

Subsampling u_subsampling (

    .Sm_in (iir_to_sub),
    .Sm_out (sub_to_fifo),
    .clk (clk),
    .clk100 (clk100),
    .sync_reset (sync_reset)
    
);
    
top_ffD u_fifo (
    .D (sub_to_fifo),
    .Q (Signal_out),
    .clk (clk100),
    .sync_reset (sync_reset),
    .fifo_connect_a (fifo_to_peak_a),
    .fifo_connect_b (fifo_to_peak_b),
    .fifo_connect_c (fifo_to_peak_c)
);

top_peak u_top_peak (

    .tpk_in0 (fifo_to_peak_a),
    .tpk_in1 (fifo_to_peak_b),
    .tpk_in2 (fifo_to_peak_c),
    .clk (clk100),
    .Ax (peak_to_class),
    .flag (flag),
    .Tk (Tk),
    .sync_reset (sync_reset)
);

assign flag_connect = flag;

Treshold u_Treshold (

    .clk (clk100),
    .S (flag_connect),
    .sync_reset (sync_reset),
    .out (Tk)
);

Classification u_classification (

    .clk (clk100),
    .flag (flag_connect),
    .sync_reset (sync_reset),
    .b (b),
    .c (c),
    .cb (cb),
    .In (peak_to_class),
    .count (count)

);

endmodule
