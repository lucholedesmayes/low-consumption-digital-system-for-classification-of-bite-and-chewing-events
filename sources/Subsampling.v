`timescale 100us / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: Sinc(i) - CONICET Sta. Fe
// Engineer: Ledesma Luciano
// 
// Create Date: 29.11.2022 09:51:53
// Design Name: 
// Module Name: Subsampling
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


//Para procesar 5 minutos de se�al a 100Hz, debo realizar 300 ciclos

module Subsampling(
    input [15:0] Sm_in,
    output reg [15:0] Sm_out,
    output reg clk100,
    input clk,
    input sync_reset
    );

integer count = 0;

always @(posedge clk) 
begin
      if(sync_reset==1'b1) begin
        clk100 <= 1'b0;
        Sm_out <= 16'b0;
        count <= 0;
      end  
      else  begin
            if(count==19) begin
            clk100 <= 1'b1;
            Sm_out <= Sm_in;
            count <= 0;
            end
            else begin
            clk100 <= 1'b0;
            Sm_out <= 0;
            count <= count+1;
            end
      end
end
endmodule