`timescale 100us / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 12.12.2022 10:31:01
// Design Name: 
// Module Name: Treshold
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module Treshold
#(
        parameter U     = 16'd1700, //valor medio 
        parameter Umin    = 16'd800,   // aprox %80 del maximo valor de la minima senial
        parameter div = 100, //tiempo de caida de treshold
        parameter te = 40 //mitad de duracion de un evento
 )
(
    input clk,
    input S,
    input S2,  
    input sync_reset,
    output reg [15:0] out = Umin
    );
    
    integer counth = 0;
    integer countc = 0;   // aproximadamente 110 ciclos de reloj entre el final de un evento y el comienzo de otro
    
    
    
always @(posedge clk) 
begin    
    
    if (S) begin
        counth = te; 
        countc = div;
    
    end
    
    
            if(counth > 0) begin
            out <= U;  //valor medio de la mayor senial  
            counth <= counth - 1;
            end
        
            else begin
            
                if (countc > 0 && counth == 0) begin
                out <= (out - ((U-Umin)/(div-1)));
                countc <= countc - 1;
                end
                
                else begin
                out <= Umin;
                end
                
            end
        
    
end
    
    
endmodule
