`timescale 100us / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: Sinc(i) - CONICET Sta. Fe
// Engineer: Ledesma Luciano
// 
// Create Date: 11.11.2022 10:20:41
// Design Name: 
// Module Name: top_ffD
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module top_ffD(
    input [15:0] D,
    output [15:0] Q,
    input clk,
    input sync_reset,
    output [15:0] fifo_connect_a,
    output [15:0] fifo_connect_b,
    output [15:0] fifo_connect_c
    );

wire [3:0] fifo_connect0;
wire [3:0] fifo_connect1;
wire [3:0] fifo_connect2;
wire [3:0] fifo_connect3;
wire [3:0] fifo_connect4;
wire [3:0] fifo_connect5;
wire [3:0] fifo_connect6;
wire [3:0] fifo_connect7;
wire [3:0] fifo_connect8;
wire [3:0] fifo_connect9;
wire [3:0] fifo_connect10;
wire [3:0] fifo_connect11;
wire [3:0] fifo_connect12;
wire [3:0] fifo_connect13;
wire [3:0] fifo_connect14;
wire [3:0] fifo_connect15;

assign fifo_connect_a = {fifo_connect15[3],fifo_connect14[3],fifo_connect13[3],fifo_connect12[3],fifo_connect11[3],
    fifo_connect10[3],fifo_connect9[3],fifo_connect8[3],fifo_connect7[3],fifo_connect6[3],fifo_connect5[3],
    fifo_connect4[3],fifo_connect3[3],fifo_connect2[3],fifo_connect1[3],fifo_connect0[3]};

assign fifo_connect_b = {fifo_connect15[2],fifo_connect14[2],fifo_connect13[2],fifo_connect12[2],fifo_connect11[2],
    fifo_connect10[2],fifo_connect9[2],fifo_connect8[2],fifo_connect7[2],fifo_connect6[2],fifo_connect5[2],
    fifo_connect4[2],fifo_connect3[2],fifo_connect2[2],fifo_connect1[2],fifo_connect0[2]};
    
assign fifo_connect_c = {fifo_connect15[1],fifo_connect14[1],fifo_connect13[1],fifo_connect12[1],fifo_connect11[1],
    fifo_connect10[1],fifo_connect9[1],fifo_connect8[1],fifo_connect7[1],fifo_connect6[1],fifo_connect5[1],
    fifo_connect4[1],fifo_connect3[1],fifo_connect2[1],fifo_connect1[1],fifo_connect0[1]};
//Instance fifo

fifo
u_fifo_0
(
.d_in (D[0]),
.d_op (Q[0]),
.clk (clk),
.sync_reset(sync_reset),
.fifo_connect (fifo_connect0)
);

fifo
u_fifo_1
(
.d_in (D[1]),
.d_op (Q[1]),
.clk (clk),
.sync_reset(sync_reset),
.fifo_connect (fifo_connect1)
);

fifo
u_fifo_2
(
.d_in (D[2]),
.d_op (Q[2]),
.clk (clk),
.sync_reset(sync_reset),
.fifo_connect (fifo_connect2)
);

fifo
u_fifo_3
(
.d_in (D[3]),
.d_op (Q[3]),
.clk (clk),
.sync_reset(sync_reset),
.fifo_connect (fifo_connect3)
);

fifo
u_fifo_4
(
.d_in (D[4]),
.d_op (Q[4]),
.clk (clk),
.sync_reset(sync_reset),
.fifo_connect (fifo_connect4)
);

fifo
u_fifo_5
(
.d_in (D[5]),
.d_op (Q[5]),
.clk (clk),
.sync_reset(sync_reset),
.fifo_connect (fifo_connect5)
);

fifo
u_fifo_6
(
.d_in (D[6]),
.d_op (Q[6]),
.clk (clk),
.sync_reset(sync_reset),
.fifo_connect (fifo_connect6)
);

fifo
u_fifo_7
(
.d_in (D[7]),
.d_op (Q[7]),
.clk (clk),
.sync_reset(sync_reset),
.fifo_connect (fifo_connect7)
);

fifo
u_fifo_8
(
.d_in (D[8]),
.d_op (Q[8]),
.clk (clk),
.sync_reset(sync_reset),
.fifo_connect (fifo_connect8)
);

fifo
u_fifo_9
(
.d_in (D[9]),
.d_op (Q[9]),
.clk (clk),
.sync_reset(sync_reset),
.fifo_connect (fifo_connect9)
);

fifo
u_fifo_10
(
.d_in (D[10]),
.d_op (Q[10]),
.clk (clk),
.sync_reset(sync_reset),
.fifo_connect (fifo_connect10)
);

fifo
u_fifo_11
(
.d_in (D[11]),
.d_op (Q[11]),
.clk (clk),
.sync_reset(sync_reset),
.fifo_connect (fifo_connect11)
);

fifo
u_fifo_12
(
.d_in (D[12]),
.d_op (Q[12]),
.clk (clk),
.sync_reset(sync_reset),
.fifo_connect (fifo_connect12)
);

fifo
u_fifo_13
(
.d_in (D[13]),
.d_op (Q[13]),
.clk (clk),
.sync_reset(sync_reset),
.fifo_connect (fifo_connect13)
);

fifo
u_fifo_14
(
.d_in (D[14]),
.d_op (Q[14]),
.clk (clk),
.sync_reset(sync_reset),
.fifo_connect (fifo_connect14)
);

fifo
u_fifo_15
(
.d_in (D[15]),
.d_op (Q[15]),
.clk (clk),
.sync_reset(sync_reset),
.fifo_connect (fifo_connect15)
);
endmodule
