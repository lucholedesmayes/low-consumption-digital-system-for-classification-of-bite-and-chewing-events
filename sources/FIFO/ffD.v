`timescale 100us / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: Sinc(i) - CONICET
// Engineer: Ledesma Luciano
// 
// Create Date: 11.11.2022 10:20:41
// Design Name: Rising edge, flip flop type D with synchronus reset
// Module Name: ffD
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////

module ffD (
    output reg op, 
    input wire ip, 
    input wire clk, 
    input wire sync_reset);

always @(posedge clk) 
begin
      if(sync_reset==1'b1)
        op <= 1'b0;
      else  
        op <= ip;
end

endmodule
