`timescale 100us / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 14.12.2022 12:09:44
// Design Name: 
// Module Name: Classification
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module Classification
#(
        parameter range = 110 //rango de medicion despues de un flag 
 )
 (
    input clk,
    input flag,
    input sync_reset,
    output [7:0] b,
    output [7:0] c,
    output [7:0] cb,
    output [7:0] count,
    input  [15:0] In
    );
    
    integer count_time=range;
    integer start;
    integer aux=0;
    integer aux_c=0;
    integer aux_b=0;
    integer aux_cb=0;
    integer aux_in;
    
always @(posedge clk) 
begin    
    if (flag)
    start=1;
    
if(start) begin
    
    if (count_time > 0) begin
    count_time <= count_time - 1;
     
        if(flag) begin
            aux = aux+1;
            if(aux==1) begin
            aux_in = In;
            end
        end    
    end    
    
    if (count_time == 0) begin
start=0;
        if(aux==1) begin
            if(aux_in>16'd2700) begin
            aux_b = aux_b + 1;
            aux = 0;
            count_time = range;
            end
            else begin
            aux_c = aux_c + 1;
            aux = 0;
            count_time = range;
            end
            end
        
        else 
        if(aux>1) begin
        aux_cb = aux_cb + 1;
        aux = 0;
        count_time = range;
        end
        else begin        
        count_time = range;
        end
        
    end   
end
end

assign b = aux_b;
assign c = aux_c;
assign cb = aux_cb;
assign count = count_time;
   
endmodule
