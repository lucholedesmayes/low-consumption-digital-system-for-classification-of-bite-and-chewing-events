`timescale 100us / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: Sinc(i) - CONICET Sta. Fe
// Engineer: Ledesma Luciano
// 
// Create Date: 28.11.2022 11:43:18
// Design Name: 
// Module Name: Abs
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module Abs(
    input [15:0] abs_in,
    output reg [15:0] abs_out,
    input sync_reset,
    input clk
    );

    reg [15:0] aux;
always @(posedge clk) 
begin
      if(sync_reset==1'b1)
        abs_out <= 16'b0;
      
      else  begin
        
        if (abs_in[15]==1'b0)
        abs_out <= abs_in;
        
        else 
        abs_out <= ~abs_in + 1;
       
      end
end

endmodule