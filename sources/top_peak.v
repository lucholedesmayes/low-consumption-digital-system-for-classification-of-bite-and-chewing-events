`timescale 100us / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 29.11.2022 11:03:29
// Design Name: 
// Module Name: top_peak
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module top_peak(
    input [15:0] tpk_in0,
    input [15:0] tpk_in1,
    input [15:0] tpk_in2,
    input [15:0] Tk,
    output reg flag = 0,
    output reg S2 = 0,
    output reg [15:0] Ax,
    output reg [15:0] Bx,
    input clk,
    input sync_reset
    );
    
    reg [15:0] A;
    reg [15:0] B;
    reg [15:0] C;
    reg [15:0] aux = 16'h0000;
    reg flag2 = 1;
    integer count=0;
        
       
    always @(posedge clk) 
begin
    if(count>=125) begin
 
    A <= tpk_in0;
    B <= tpk_in1;
    Ax <= A;
    
    if (aux < B)
    flag2 <= 1'b1;
    else
    flag2 <= 1'b0;

        if (B>16'd900 && B<16'd8000 && B>A && flag==0 && flag2==1) begin   
        
            if(A>Tk) begin
                
            flag <= 1'b1;
            flag2 <= 1'b0;
            aux <= B;
        
            end
        end
        
        else
        flag <= 1'b0;
        aux <= B;      
        end    
    
    else
    count <= count+1;

end
endmodule
